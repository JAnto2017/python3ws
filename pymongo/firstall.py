#The find() method returns all occurrences in the selection.

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

for x in mycol.find():
	print(x)