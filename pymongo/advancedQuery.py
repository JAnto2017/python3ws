#find documents where the address starts with the letter "S" or higher

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

myquery = {"address":{"$gt":"S"}}

mydoc = mycol.find(myquery)

for x in mydoc:
	print(x)