#The find() method returns all occurrences in the selection.
#Return only the names and address not the _ids

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

for x in mycol.find({},{"_id":0,"nombre":1,"direccion":1}):
	print(x)

"""
You are not allowed to specify both 0 and 1 values in the same object (except if one of the fields is the _id field). If you specify a field with the value 0, all other fields get the value 1, and vice versa:
"""