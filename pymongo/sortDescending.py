# Sort the result alphabetically by name
# The sort() method takes one parameter for 'fieldname' and one parameter for 'direction'

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]


mydoc = mycol.find().sort("nombre")



for x in mydoc:
	print(x)