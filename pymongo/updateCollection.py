# Update a record
# Change the address from "Valley 345" to "Canyon 123"

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]


myquery = {"direccion":"Valley 345"}
newvalues = {"$set": {"direccion":"Canyon 123"}}

mycol.update_one(myquery, newvalues)

#imprimir después de actualizar
for x in mycol.find():
	print(x)