#insert multiple documents into a collection in MongoDB, we use the insert_many()

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

mylist = [{"nombre":"Amy","direccion":"Apple st 652"},{"nombre":"Hannah","direccion":"Mountain 21"},{"nombre":"Michael","direccion":"Valley 345"},{"nombre":"Sandy","direccion":"Ocean blud 2"},{"nombre":"Betty","direccion":"Green Grass 1"},{"name":"Richard","direccion":"Sky st 331"},{"nombre":"Susan","direccion":"One way 98"},{"nombre":"Vicky","direccion":"Yellow Garden 2"},{"nombre":"Ben","direccion":"Park Lane 38"},{"nombre":"William","direccion":"Central st 954"},{"nombre":"Chuck","direccion":"Main Road 989"},{"nombre":"Viola","direccion":"Sideway 1633"}]

x = mycol.insert_many(mylist)

#print list of the _id values of the inserted documents
print(x.inserted_ids)