#Find documents with the address Park Lane 38
#The first argument of the find() method is a query object, and is used to limit the search

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

myquery = {"address":"Park Lane 38"}

mydoc = mycol.find(myquery)

for x in mydoc:
	print(x)