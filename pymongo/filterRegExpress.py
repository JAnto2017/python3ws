#find documents where the address starts with the letter "S"
#regular expressions can only be used to query strings

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

myquery = {"address":{"$regex":"^S"}}

mydoc = mycol.find(myquery)

for x in mydoc:
	print(x)