#creating a collection

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]

mycol = mydb["clientes"]

#return a list of all collections in your database:--------------
print(mydb.list_collection_names())

#check if the "clientes" collections exists ---------------------
collist = mydb.list_collection_names()
if "clientes" in collist:
	print("The collection exists.")