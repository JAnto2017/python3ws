import pymongo

#MongoDB will create the database if it does not exist, and make a connection to it

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["mydatabasemongo"]

"""
Important: in MongoDB a database is not created until it gets content!
MongoDB waits until you have created a collection (table) with al least one document (record) before it actually creates the database (and collection)
"""

print(myclient.list_database_names())
#you can check if a database exist by listing all databases in you system.
#return a list of your system's databases:

dblist = myclient.list_database_names()
if "mydatabasemongo" in dblist:
	print("The databasee exists - mydatabasemongo -")

#check if mydatabasemongo exists