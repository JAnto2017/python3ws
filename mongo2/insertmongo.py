#Insert a record in the "clientes" collection

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]

mydict = {"nombre":"José Antonio", "direccion":"Bicacarera 7"}

x = mycol.insert_one(mydict)

print(x.inserted_id)