# Limit the result to only return 5 documents
# The limit() method takes one parameter, a number defining how many documents to return.

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabasemongo"]
mycol = mydb["clientes"]


myresult = mycol.find().limit(5)

# imprimir resultados
for x in myresult:
	print(x)