#acceso a BD SQL
#return a list of your system's databases 

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password"
)

mycursor = mydb.cursor()

mycursor.execute("SHOW DATABASES")

for x in mycursor:
	print(x)