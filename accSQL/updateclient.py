#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()


sql = "UPDATE clientes SET nombre='José Antonio' WHERE id=1"

mycursor.execute(sql)

mydb.commit()
"""
Important: notice the statement mydb.commit() it is required to make the changes, otherwise no changes are made to the table.
"""

print(mycursor.rowcount,"record(s) affected")