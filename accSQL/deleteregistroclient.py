#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()


sql = "DELETE FROM clientes WHERE id=3"

mycursor.execute(sql)

mydb.commit()
"""
Important!: Notice the statement mydb.commit() it is required to make the changes, otherwise no changes are made to the table.
"""
print(mycursor.rowcount, "record(s) deleted")