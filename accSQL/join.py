#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()


sql = "SELECT alumnos.nombre AS alum_, clientes.nombre AS favorit_ FROM alumnos RIGHT JOIN clientes ON alumnos.direccion = clientes.id"

"""
INNER	JOIN
LEFT	JOIN
RIGHT 	JOIN
"""

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
	print(x)