#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()


sql = "SELECT * FROM clientes LIMIT 2 OFFSET 1"

#start from position 2, and return 5 records

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
	print(x)