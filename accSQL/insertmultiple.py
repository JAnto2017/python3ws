#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()

sql = "INSERT INTO alumnos (nombre,direccion) VALUES (%s,%s)"
val = [("Edu","La rioja 2"),("Anibal","La fuente 5"),("Sara","Las estrellas 13")]

mycursor.executemany(sql, val)

mydb.commit()
"""
important!: Notice the statement mydb.commit() it is required to make the changes, otherwise no changes are made to the table
"""
print(mycursor.rowcount,"record inserted.")
print("inserted ID: ",mycursor.lastrowid)
