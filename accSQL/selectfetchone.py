#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()


mycursor.execute("SELECT * FROM alumnos")

myresult = mycursor.fetchone()

"""
If you are only interested in one row, you can use the fetchone() method. The fetchone() method will return the first row of the result:
"""

print(myresult)
