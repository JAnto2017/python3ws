#create a table named 'clientes'

import mysql.connector

mydb = mysql.connector.connect(
	host="localhost",
	user="admin",
	password="password",
	database="databasepython"
)

mycursor = mydb.cursor()

sql = "INSERT INTO clientes (nombre,direccion) VALUES (%s,%s)"
val = ("Gorrín","Conserjería")

mycursor.execute(sql, val)

mydb.commit()
"""
important!: Notice the statement mydb.commit() it is required to make the changes, otherwise no changes are made to the table
"""
print(mycursor.rowcount,"record inserted.")
print("inserted ID: ",mycursor.lastrowid)
