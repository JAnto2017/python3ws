print("Hello, world!")

x=5
print('esto es una variable x = ',x)

y=1
print('Esto es un casting a float desde int y = ',float(y))

z=3+5j
print('Esto es un número complejo z = ',z)

s=3
print('Esto es un casting de int a string ',str(s))
