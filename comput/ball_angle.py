#Python program with a library function

from math import atan,pi

x = 10
y = 10

angle = atan(y/x)
soluc = (angle/pi) * 180

print(soluc)